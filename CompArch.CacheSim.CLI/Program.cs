﻿using System;
using CompArch.CacheSim;

namespace CompArch.CacheSim.CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            // default memory with no offset
            var memory = new ArrayReadonlyMemory(new uint[] {
                0x92, 0x70, 0x8C, 0xFD, 0xB9, 0xE2, 0x40, 0xC2, 0x0D, 0x9A, 0xD1, 0xF8, 0x43, 0x7E, 0xB7, 0x75,
                0xFB, 0x44, 0xDD, 0xF6, 0xA6, 0x43, 0x11, 0x17, 0x98, 0x88, 0x08, 0x6A, 0x6D, 0xB8, 0xBC, 0x12,
                0x0A, 0xF1, 0x4C, 0x45, 0x63, 0x2C, 0x40, 0x98, 0x91, 0x65, 0x0E, 0x76, 0xEE, 0x5D, 0x18, 0x29,
                0x85, 0x13, 0x60, 0xC5, 0x56, 0xF2, 0x89, 0x9E, 0x06, 0xE2, 0x0B, 0xA2, 0xB2, 0x41, 0xB1, 0x7B
            }, 0);
            var cache = new TwoWaySetAssociativeCache(memory);

            Console.WriteLine("Cache Simulator");
            Console.WriteLine("by Micah Switzer 3-2018");
            PrintUsage();
            Console.WriteLine($"Memory contains data from {memory.Offset} to {memory.Size + memory.Offset - 1}");
            
            while (true)
            {
                Console.Write(" > ");
                string input = Console.ReadLine();
                uint memAddress;
                var result = uint.TryParse(input, out memAddress);
                if (!result) 
                {
                    if (input.Trim().ToUpper() == "E") break;
                    else {
                        Console.WriteLine("Please enter a decimal address");
                        continue;
                    }
                }
                var inRange = memory.IsValidAddress(memAddress);
                if (!inRange)
                {
                    Console.WriteLine($"Address {memAddress} is not in the valid range of addresses");
                    continue;
                }
                uint memValue;
                var cacheHit = cache.GetValue(memAddress, out memValue);
                Console.Clear();
                Console.Write(cacheHit ? "Cache HIT" : "Cache MISS");
                Console.WriteLine($" for address {memAddress}, value = 0x{memValue.ToString("X")}");
                PrintCache(cache.Dump());
            }

            Console.WriteLine("Exiting...");
        }

        private static void PrintUsage()
        {
            Console.WriteLine("Usage:");
            Console.WriteLine(" > [address] to retrieve value at that address");
            Console.WriteLine(" > \"e\" to exit");
        }

        private static void PrintCache(CacheEntry[,] sets)
        {
            int tableRows = sets.GetLength(0) + 1;
            int tableWidth = sets.GetLength(1) * 2 + 1;
            var table = new string[tableRows, tableWidth];
            table[0, 0] = "set";
            for (int i = 1; i < tableWidth; i += 2)
            {
                table[0, i] = "tag";
                table[0, i + 1] = "value";
            }
            for (int i = 1; i < tableRows; i++)
            {
                table[i, 0] = (i - 1).ToString();
                for (int j = 0; j < sets.GetLength(1); j++)
                {
                    var item = sets[i - 1, j];
                    var hasValue = item.LastUpdated != 0;
                    table[i, 1 + j * 2] = hasValue ? item.Tag.ToString() : "-";
                    table[i, 2 + j * 2] = hasValue ? item.Value.ToString("X2") : "-";
                }
            }
            PrintTable(table);
        }

        private static void PrintTable(string[,] table) 
        {
            // compute widths of columns
            int[] widths = new int[table.GetLength(1)];
            for (int col = 0; col < table.GetLength(1); col++)
            {
                for (int row = 0; row < table.GetLength(0); row++)
                {
                    int length = table[row, col].Length;
                    if (widths[col] < length)
                        widths[col] = length;
                }
            }
            // print each row
            for (int i = 0; i < table.GetLength(0); i++)
            {
                PrintDivider(widths);
                for (int j = 0; j < table.GetLength(1); j++)
                {
                    Console.Write($"| {table[i, j]}");
                    PrintMultiple(' ', widths[j] - table[i, j].Length + 1);
                }
                Console.WriteLine('|');
            }
            PrintDivider(widths);
        }
        
        private static void PrintDivider(int[] widths)
        {
            for (int i = 0; i < widths.Length; i++)
            {
                Console.Write('+');
                PrintMultiple('-', widths[i] + 2);
            }
            Console.WriteLine('+');
        }

        private static void PrintMultiple(char ch, int count)
        {
            for (int i = 0; i < count; i++)
                Console.Write(ch);
        }
    }
}
