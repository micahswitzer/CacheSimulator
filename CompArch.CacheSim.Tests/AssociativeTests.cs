using Microsoft.VisualStudio.TestTools.UnitTesting;
using CompArch.CacheSim;
using System;

namespace CompArch.CacheSim.Tests
{
    [TestClass]
    public class AssociativeTests
    {
        readonly uint[] rawData = new uint[] {
            0, 10, 20, 30, 40, 50, 60, 70, 80, 90
        };
        readonly IReadonlyMemory memory;

        public AssociativeTests()
        {
            memory = new ArrayReadonlyMemory(rawData, 0);
        }

        [TestMethod]
        public void CreateCache()
        {
            ICache cache = new TwoWaySetAssociativeCache(memory);
            Assert.IsNotNull(cache);
        }

        [TestMethod]
        public void GetValue()
        {
            const int IDX = 5;
            ICache cache = new TwoWaySetAssociativeCache(memory);
            
            uint value;
            
            var result = cache.GetValue(IDX, out value);
            Assert.IsFalse(result);
            Assert.AreEqual(rawData[IDX], value);
            
            result = cache.GetValue(IDX, out value);
            Assert.IsTrue(result);
            Assert.AreEqual(rawData[IDX], value);
        }
    }
}
