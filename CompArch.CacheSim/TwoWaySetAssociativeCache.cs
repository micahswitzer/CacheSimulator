﻿using System;

namespace CompArch.CacheSim
{
    public class TwoWaySetAssociativeCache : ICache
    {
        protected const uint SET_HEIGHT = 4;
        protected const uint SET_WIDTH = 2;
        protected const int SET_BITS = 2;
        
        protected readonly IReadonlyMemory memory;
        protected readonly CacheEntry[,] sets = new CacheEntry[SET_HEIGHT, SET_WIDTH];
        protected uint updateNumber = 0;

        public TwoWaySetAssociativeCache(IReadonlyMemory memory)
        {
            this.memory = memory;
        }

        public bool GetValue(uint address, out uint value)
        {
            // make sure the provided value is in range
            if (!memory.IsValidAddress(address)) throw new IndexOutOfRangeException();
            updateNumber++; // don't bother incrementing the update number if the address is bad
            (uint set, uint tag) = GetSetAndTag(address);
            int oldestEntryIdx = 0;
            for (int t = 0; t < SET_WIDTH; t++) 
            {
                var tempEntry = sets[set, t];
                if (tempEntry.LastUpdated == 0 || tempEntry.Tag != tag) {
                    // find the oldest entry in this set in case we need to replace it
                    if (tempEntry.LastUpdated < sets[set, oldestEntryIdx].LastUpdated) 
                        oldestEntryIdx = t;
                    continue;
                }
                // cache hit
                value = tempEntry.Value;
                return true;
            }
            // cache miss
            value = CacheValue(address, oldestEntryIdx).Value;
            return false;
        }

        public bool IsValueCached(uint address)
        {
            throw new NotImplementedException();
        }

        public CacheEntry[,] Dump()
        {
            //var copy = new CacheEntry[SET_HEIGHT, SET_WIDTH];
            //sets.CopyTo(copy, 0);
            return sets; // DANGEROUS
        }

        protected CacheEntry CacheValue(uint address, int setIndex)
        {
            var val = memory.GetValue(address);
            (uint set, uint tag) = GetSetAndTag(address);
            var newEntry = new CacheEntry {
                LastUpdated = updateNumber,
                Tag = tag,
                Value = val
            };
            sets[set, setIndex] = newEntry;
            return newEntry;
        }

        protected (uint set, uint tag) GetSetAndTag(uint address) =>
            (~(uint.MaxValue << SET_BITS) & address, address >> SET_BITS);
    }
}
