namespace CompArch.CacheSim 
{
    public struct CacheEntry
    {
        public uint Tag;
        public uint Value;
        public uint LastUpdated;
    }
}
