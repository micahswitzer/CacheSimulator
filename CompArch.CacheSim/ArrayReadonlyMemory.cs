using System;

namespace CompArch.CacheSim
{
    /// <summary>
    /// Basic memory implementation using an array
    /// </summary>
    public class ArrayReadonlyMemory : IReadonlyMemory
    {
        protected readonly uint offset = 0;
        public uint Offset => offset;
        protected readonly uint[] data;
        public uint Size => (uint)data.Length; // data will never be null so no need for null checks

        public ArrayReadonlyMemory(uint[] data, uint offset = 0)
        {
            if (data == null) throw new ArgumentNullException(nameof(data));
            this.data = data;
            this.offset = offset;
        }

        public uint GetValue(uint address)
        {
            if (!IsValidAddress(address)) throw new IndexOutOfRangeException();
            return data[address - offset];
        }

        public bool IsValidAddress(uint address)
        {
            return (address >= offset && address < Size + offset);
        }
    }
}