using System;

namespace CompArch.CacheSim
{
    public interface IReadonlyMemory
    {
        uint GetValue(uint address);
        bool IsValidAddress(uint address);
    }
}
