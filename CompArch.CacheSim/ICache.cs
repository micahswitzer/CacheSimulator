using System;

namespace CompArch.CacheSim
{
    /// <summary>
    /// Represents a generic cache interface
    /// </summary>
    public interface ICache
    {
        /// <summary>
        /// Attempts to get the value from cache, caching it if it isn't already there
        /// </summary>
        /// <param name="address">The address of the value to retrieve</param>
        /// <param name="value">The variable to output the value to</param>
        /// <returns>Whether or not the value was already in cache</returns>
        bool GetValue(uint address, out uint value);
        /// <summary>
        /// Gets whether or not the value at the given address is in the cache
        /// </summary>
        /// <param name="address">The address to check</param>
        /// <returns></returns>
        bool IsValueCached(uint address);
    }
}
